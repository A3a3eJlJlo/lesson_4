﻿function isEven(Number)
{
	Number = Number > 0 ? Number : -Number;
	if(Number === 0)
		return true;
	if(Number === 1)
		return false;
  
	return isEven(Number - 2);
}

// Без проверки на отрицательность, функция съедает весь стэк,
// т.к. нужное услови не будет никогда достигнуто, 
// мы верными шагами будем идти к минус бесконечности.
console.log(isEven(-1));