function countBs(Text)
{
	var Counter = 0;
	for(var i = 0; i < Text.length; i++)
	{
		if(Text.charAt(i) == "B")
			++Counter;
	}
	return Counter;
}

function countSymbol(Text, Symbol)
{
	var Counter = 0;
	for(var i = 0; i < Text.length; i++)
	{
		if(Text.charAt(i) == Symbol)
			++Counter;
	}
	return Counter;
}